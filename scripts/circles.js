let rgba1, rgba2, liGrad;
let container = document.querySelector(".circles-container");

for (let i = 0 ; i<505;i++)
{
    function randColor(){
        let ranVal = Math.floor(Math.random()* 256);
        return ranVal;
    }

    function randDeg(){
        let ranDeg = Math.floor(Math.random()*91)   //Math.random([45,30,90,60,45,45]);
        return ranDeg;
    }

    rgba1 = "rgba("+randColor()+','+randColor()+','+randColor()+")";
    rgba2 = "rgba("+randColor()+','+randColor()+','+randColor()+")";
    liGrad = "linear-gradient("+randDeg()+'deg ,'+rgba1+','+rgba2+')';

    let div = document.createElement('div')
    div.className = "circle";
    div.style.backgroundColor = `${rgba1}`;
    div.style.backgroundImage = `${liGrad}`;
    container.appendChild(div);
}
